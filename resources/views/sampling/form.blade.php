<fieldset>
	
	<div class="row">
		<div class="col-md-12">
	<div class="form-group">
		{!! Form::label('Nada','Nada') !!}
		{!! Form::select('nada',['C'=>'C','C#'=>'C#','D'=>'D','D#'=>'D#','E'=>'E','F'=>'F','F#'=>'F#','G'=>'G','G#'=>'G#','A'=>'A','A#'=>'A#','B'=>'B'],null,['class'=>'form-control']) !!}
		{!! $errors->first('nada','<dt style="color:red;" >:message</dt>'); !!}		
	</div>
	
	

	
</div>
<div class="col-md-6">
	<div class="form-group">
		{!! Form::label('Sampling','Sampling') !!}
		{!! Form::text('sampling',null,['class'=>'form-control']) !!}
		{!! $errors->first('sampling','<dt style="color:red;" >:message</dt>'); !!}		
	</div>
	
</div>
<div class="col-md-6">
	<div class="form-group">
		{!! Form::label('Diapason','Diapason') !!}
		{!! Form::text('diapason',null,['class'=>'form-control']) !!}
		{!! $errors->first('diapason','<dt style="color:red;" >:message</dt>'); !!}		
	</div>
</div>
</div>
</fieldset>

