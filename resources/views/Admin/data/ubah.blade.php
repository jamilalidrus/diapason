@extends('template.admin.main-template')

@section("content")
	
	{!! Form::model($admin,['route'=>['data.update',$admin->id],'method'=>'put','enctype'=>'multipart/form-data']) !!}
		@include('Admin.data.form')

	{!! Form::close() !!}

@stop()