@extends('template.admin.main-template')

@section("content")
	<table border="1">
		<tr>
			<td>No</td>
			<td>Nama</td>
			<td>Email</td>
			<td>Gambar</td>
			<td>Aksi</td>
		</tr>
		<tr>
		@php
			$no=1;
		@endphp
			@foreach($admin as $adm)
				<td>{{$no++}}</td>
				<td>{{$adm->nama}}</td>
				<td>{{$adm->email}}</td>
				<td><img width="150px;" src="{{asset("gambar/admin/$adm->gambar")}}"></td>
				<td>
				{!! Form::open(['route'=>['data.destroy',$adm->id],'method'=>'delete']) !!}
					<a href="{{route('data.edit',$adm->id)}}">Ubah</a>
					<button type="submit" >Hapus</button>

				
				
				{!! Form::close() !!}</td>
			@endforeach()
		</tr>
	</table>
@stop()