@extends('template.admin.main-template')

@section("content")

	{!! Form::open(['route'=>'data.store','method'=>'post','enctype'=>'multipart/form-data']) !!}

				@include('Admin.data.form')

	{!! Form::close() !!}

@stop()