@extends('template.admin.template-admin')

@section("content")
	<div class="row">
  <div class="col-md-12">
    <div class="ibox float-e-margins">
      <div class="ibox-title">
        <h5>Data Admin</h5>

        <div class="ibox-tools">
          <a class="collapse-link">
            <i class="fa fa-chevron-up"></i>
          </a>
          <a class="dropdown-toggle" data-toggle="dropdown" href="#">
            <i class="fa fa-wrench"></i>
          </a>
          <ul class="dropdown-menu dropdown-user">
            <li><a href="#">Config option 1</a>
            </li>
            <li><a href="#">Config option 2</a>
            </li>
          </ul>
          <a class="close-link">
            <i class="fa fa-times"></i>
          </a>
        </div>
      </div>
      <div class="ibox-content">
      <div>
      <a href="{{route('sampling.create')}}" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah data</a>
      </div>
          @if(Session::has('message'))
          <div class="alert alert-success alert-dismissable">
            
            <dt style="font-family:verdana;"><i class="fa fa-check"></i>  {{Session::get('message')}}</dt>
    
          </div>  
          @endif

        <div class="table-responsive">
          <table class="table table-striped table-bordered table-hover dataTables-example" >
            <thead>
            <?php  $no=1; ?>
              <tr>

                <th>No</th>
                <td>Oktav</td>

              </tr>
            </thead>
            <tbody>
              <?php $no=1; ?>
              @foreach($oktav as $ok)
              <tr class="gradeC">              
                <td>{{$no++}}</td>             
                <td><a href="{{route('sampling.index',[$alat->id,$ok->id])}}" class="btn btn-primary" >{{$ok->oktav}}</a></td>
              </tr>                  
              @endforeach()
              <tr>
                
                <td colspan="2" ><a href="{{route('allsampling.index',$alat->id)}}" class="btn btn-primary btn-block">Semua</a></td>
              </tr>
            </tbody>
            <tfoot>
              <tr>
                <th>No</th>
                <td>Oktav</td>
               
              </tr>
            </tfoot>
          </table>
          
        </div>

      </div>
    </div>
  </div>
</div>
@stop()