@extends('template.admin.template-admin')

@section("content")
	<div class="row">
  <div class="col-md-12">
    <div class="ibox float-e-margins">
      <div class="ibox-title">
        <h5>Data Admin</h5>

        <div class="ibox-tools">
          <a class="collapse-link">
            <i class="fa fa-chevron-up"></i>
          </a>
          <a class="dropdown-toggle" data-toggle="dropdown" href="#">
            <i class="fa fa-wrench"></i>
          </a>
          <ul class="dropdown-menu dropdown-user">
            <li><a href="#">Config option 1</a>
            </li>
            <li><a href="#">Config option 2</a>
            </li>
          </ul>
          <a class="close-link">
            <i class="fa fa-times"></i>
          </a>
        </div>
      </div>
      <div class="ibox-content">
      <div>
      <a href="{{route('alat.create')}}" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah data</a>
      </div>
          @if(Session::has('message'))
          <div class="alert alert-success alert-dismissable">
            
            <dt style="font-family:verdana;"><i class="fa fa-check"></i>  {{Session::get('message')}}</dt>
    
          </div>  
          @endif

        <div class="table-responsive">
          <table class="table table-striped table-bordered table-hover dataTables-example" >
            <thead>
            <?php  $no=1; ?>
              <tr>

                <th>No</th>
                <th>Alat</th>             
                <th>Aksi</th>

              </tr>
            </thead>
            <tbody>
              <?php $no=1; ?>
              @foreach($musik as $sp)
              <tr class="gradeC">              
                <td>{{$no++}}</td>             
                <td>{{$sp->nama}}</td>                      
                <td>
                  <!-- {!! Form::open(['route'=>['sampling.destroy',$sp->id],'method'=>'delete','onsubmit'=>'return confirm("Yakin Ingin Menghapus?")'])!!} -->
                  <a href="{{route('alat.show',$sp->id)}}" class="btn btn-primary">View</a>
                 <!--  <a class="btn btn-warning" href="{{route('sampling.edit',$sp->id)}}"><i class="fa fa-pencil"></i> Ubah</a>
                  <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i>  Hapus</button> -->
                  {!! Form::close()!!}
                </td>       
                
              </tr>           
                 
                  
          @endforeach()
            </tbody>
            <tfoot>
              <tr>
                <th>No</th>
                <th>Alat</th>             
                <th>Aksi</th>
              </tr>
            </tfoot>
          </table>
          
        </div>

      </div>
    </div>
  </div>
</div>
@stop()