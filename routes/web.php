<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','AlatMusikController@index');

Route::Resources([
	'oktav'=>'OktavController',
	'alat'=>'AlatMusikController',
	'sampling'=>'SamplingController',
	]);

Route::get('tambahnada/{id}/{other}/create','SamplingController@create')->name('sampling.buat');
Route::post('sampling/{id}/{other}/simpan','SamplingController@store')->name('sampling.store');
Route::get('sampling/{id}/{other}/index','SamplingController@index')->name('sampling.index');
Route::get('sampling/{id}/index','SamplingController@all')->name('allsampling.index');

//ada

