<?php

namespace App\Http\Middleware;

use Closure;
use Auth;


class AdminMiddleware
{


    // protected $admin;

    // public function __construct(Guard $admin){
    //     $this->admin=$admin;
    // }
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next,$role)
    {
      switch ($role) {
        case 'admin': 
        if (Auth::guard('admin')->check()) {
          return $next($request);
        }else{
          return redirect('formlogin');   
        }               


        break;

        default:

        break;
      }
      return response(202);

    }
  }
