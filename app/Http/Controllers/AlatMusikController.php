<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\AlatMusik;
use App\Model\Oktav;
class AlatMusikController extends Controller
{
    public function __construct(AlatMusik $alat){
        $this->alat=$alat;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $musik= AlatMusik::all();
        $view=[
            'musik'=>$musik,
        ];
        return view('alatmusik.index')->with($view);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('alatmusik.tambah');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[

            'nama'=>'required'
        ]);

        $alat=$request->all();
        $data=$this->alat->create($alat);
        \Session::flash('message','alat musik berhasil di input, silahkan input nada dari setiap oktav');
        return redirect(route('alat.show',$data->id));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $alat =$this->alat->findOrFail($id);
        $oktav= Oktav::all();
        $view=[
            'alat'=>$alat,
            'oktav'=>$oktav,
    
        ];
        
        return view('alatmusik.show')->with($view);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
