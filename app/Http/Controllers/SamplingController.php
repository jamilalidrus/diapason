<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Sampling;
use App\Model\Oktav;
use Session;
class SamplingController extends Controller
{
    public function __construct(Sampling $sampling,Oktav $oktav){
    	$this->sampling=$sampling;
    	$this->oktav=$oktav;
    }

    public function index($musik_id,$oktav_id){        
        $sampling=$this->sampling->where(['alat_id'=>$musik_id,'oktav_id'=>$oktav_id])->orderBy('id','asc')->paginate(12);
        $view=[
            'sampling'=>$sampling,
            'musik_id'=>$musik_id,
            'oktav_id'=>$oktav_id,
        ];
        return view('sampling.index')->with($view);
    }
    public function show($oktav_id){
        $sampling=$this->sampling->where('oktav_id',$oktav_id)->get();
        return $sampling;
    }
     public function create($musik_id,$oktav_id){
        $view=[
            'musik_id'=>$musik_id,
            'oktav_id'=>$oktav_id,
        ];
    	return view('sampling.tambah')->with($view);
    }
    public function store(Request $request,$musik_id,$oktav_id){
    	$this->validate($request,[
            'nada'=>'required',
            'sampling'=>'required|numeric',
            'diapason'=>'required|numeric',

        ]);
        $sampling=$request->all();
        $jika = Sampling::where(['oktav_id'=>$oktav_id,'alat_id'=>$musik_id,'nada'=>$request->nada])->get();
        if (count($jika) > 0) {
            Session::flash('message','nada ini sudah ada, silahkan input dengan nada yang lain');
            return redirect()->back();
        }else{
           $x1= $request->sampling;
           $x2= $request->diapason;

           if ($x1 > $x2) {
               $x3 = ($x1 - $x2);
               $hasil=  $x3  /$x2 * 100;
           }elseif($x1 < $x2){
            $x3 = ($x2 - $x1);
            $hasil=  $x3  /$x2 * 100;
        }elseif($x1 = $x2){
            $x3 = ($x1 - $x2);
            $hasil=  $x3  /$x2 * 100;
        }  
        
        $sampling['oktav_id']=$oktav_id;      
        $sampling['alat_id']=$musik_id;  
        $sampling['persentase']=str_replace('-','',$hasil);   
        $this->sampling->create($sampling);
        \Session::flash('message','data berhasil disimpan');
        return redirect(route('sampling.index',[$musik_id,$oktav_id]));
    }    
}
     public function edit($id){
        $sampling= Sampling::find($id);
    	return view('sampling.ubah',compact('sampling'));
    }
     public function update(Request $request,$id){
        $back=$this->sampling->find($id);

    	   $this->validate($request,[
            'nada'=>'required',
            'sampling'=>'required|numeric',
            'diapason'=>'required|numeric',
           ]);
        $sampling=$request->all();
           $x1= $request->sampling;
           $x2= $request->diapason;

           if ($x1 > $x2) {
                $x3 = ($x1 - $x2);
                $hasil=  $x3  /$x2 * 100;
            }elseif($x1 < $x2){
                $x3 = ($x2 - $x1);
                $hasil=  $x3  /$x2 * 100;
            }elseif($x1 = $x2){
                $x3 = ($x1 - $x2);
                $hasil=  $x3  /$x2 * 100;
            }
        $sampling['persentase']=str_replace('-','',$hasil);   
       $param= $this->sampling->find($id)->update($sampling);
        \Session::flash('message','data berhasil diubah');
        return redirect(route('sampling.index',[$back->alat_id,$back->oktav_id]));

    }
     public function destroy($id){
    	Sampling::destroy($id);
        Session::flash('message','data berhasil dihapus');
        return redirect()->back();
    }

    public function all($id){
        $view=[
            'sampling'=>Sampling::where('alat_id',$id)->orderBy('oktav_id','asc')->get(),

        ];
       
        return view('sampling.all')->with($view);
    }
}
