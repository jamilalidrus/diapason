<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Oktav extends Model
{
    protected $table='oktav';
    protected $fillable=['oktav'];

    public function sampling(){
    	return $this->hasMany(Sampling::class,'oktav_id','id');
    }
}
