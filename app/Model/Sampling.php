<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Sampling extends Model
{
    protected $table='sampling';
    protected $fillable=['nada','sampling','diapason','persentase','oktav_id','alat_id'];
    public $timestamps=false;

    public function oktav(){
    	return $this->belongsTo(Oktav::class);
    }

    public function alat(){
    	return $this->belongsTo(AlatMusik::class);
    }
}
