<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AlatMusik extends Model
{
    protected $table='alat_musiks';
    protected $fillable = ['nama'];

    public function sampling(){
    	return $this->hasMany(Sampling::class);
    }
}
