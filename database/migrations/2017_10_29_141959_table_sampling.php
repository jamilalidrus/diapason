<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TableSampling extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('sampling')) {
            Schema::create('sampling', function (Blueprint $table) {
                $table->increments('id');
                $table->string('nada');
                $table->string('sampling');
                $table->string('diapason');
                $table->string('persentase');
                $table->string('oktav_id');
                $table->string('alat_id');
                $table->timestamps();
            });
        }
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sampling');
    }
}
