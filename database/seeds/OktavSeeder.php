<?php

use Illuminate\Database\Seeder;

class OktavSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       $oktav = DB::table('oktav')->delete();

       $data=[
       		[
       			'id'=>1,
       			'oktav'=>'oktav2',
       		],
       		[
       			'id'=>2,
       			'oktav'=>'oktav3',
       		],
       		[
       			'id'=>3,
       			'oktav'=>'oktav4',
       		],
       		[
       			'id'=>4,
       			'oktav'=>'oktav5',
       		],
       		[
       			'id'=>5,
       			'oktav'=>'oktav6',
       		],
       ];

     
        DB::table('oktav')->insert($data);
       
    }
}
